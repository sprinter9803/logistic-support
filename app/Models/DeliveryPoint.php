<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class DeliveryPoint extends Model
{
    protected $table = 'delivery_point';

    use HasFactory;

    public function getHash()
    {
        $split_fields = implode("|", [
            $this->code,
            $this->name,
            $this->work_time,
            $this->address,
            $this->phone,
            $this->note,
            $this->cx,
            $this->cy,
            $this->dressing,
            $this->cash,
            $this->station,
            $this->site,
            $this->metro,
            $this->address_comment,
            $this->city_code,
            $this->type,
            $this->additional_fields,
        ]);

        return md5($split_fields);
    }

    public static function getPotentialPointHash(array $point_data)
    {
        $split_potential_point_fields = implode("|", [
            $point_data["Code"],
            $point_data["Name"],
            $point_data["WorkTime"],
            $point_data["Address"],
            $point_data["Phone"],
            $point_data["Note"],
            $point_data["cX"],
            $point_data["cY"],
            $point_data["Dressing"],
            $point_data["Cash"],
            $point_data["Station"],
            $point_data["Site"],
            $point_data["Metro"],
            $point_data["AddressComment"],
            $point_data["CityCode"],
            $point_data["Type"],
            json_encode($point_data["AdditionalFields"])
        ]);

        return md5($split_potential_point_fields);
    }
}
