<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CdekAccessToken extends Model
{
    protected $table = 'cdek_access_token';

    use HasFactory;
}
