<?php

namespace Modules\Pvz\Http\Controllers;

use Illuminate\Contracts\Support\Renderable;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Modules\Pvz\Services\PvzService;
use Modules\Pvz\Services\PvzManageService;

class PvzController extends Controller
{
    protected $pvz_service;

    protected $pvz_manage_service;

    public function __construct(PvzService $pvz_service, PvzManageService $pvz_manage_service)
    {
        $this->pvz_service = $pvz_service;
        $this->pvz_manage_service = $pvz_manage_service;
    }

    public function getAllPvz()
    {
        return $this->pvz_service->getAllPvz();
    }

    public function getCityPvz(Request $request)
    {
        $city_name = $request->input('name');
        return $this->pvz_service->getDeliveryPointsForCity($city_name);
    }

    public function updatePvz()
    {
        return $this->pvz_manage_service->dataManageQuery();
    }
}
