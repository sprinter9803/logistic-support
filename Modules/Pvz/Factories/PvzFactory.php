<?php

namespace Modules\Pvz\Factories;

use App\Models\DeliveryPoint;

/**
 * Класс-фабрика для создания объектов модуля мониторинга
 *
 * @author Oleg Pyatin
 */
class PvzFactory
{
    /**
     * Функция для создания новой записи о пункте ПВЗ
     *
     * @return DeliveryPoint
     */
    public function createDeliveryPoint(array $delivery_point_data): DeliveryPoint
    {
        $new_delivery_point = new DeliveryPoint();

        $new_delivery_point->code = $delivery_point_data["Code"];
        $new_delivery_point->name = $delivery_point_data["Name"];
        $new_delivery_point->work_time = $delivery_point_data["WorkTime"];
        $new_delivery_point->address = $delivery_point_data["Address"];
        $new_delivery_point->phone = $delivery_point_data["Phone"];
        $new_delivery_point->note = $delivery_point_data["Note"];
        $new_delivery_point->cx = $delivery_point_data["cX"];
        $new_delivery_point->cy = $delivery_point_data["cY"];
        $new_delivery_point->dressing = $delivery_point_data["Dressing"];
        $new_delivery_point->cash = $delivery_point_data["Cash"];
        $new_delivery_point->station = $delivery_point_data["Station"];
        $new_delivery_point->site = $delivery_point_data["Site"];
        $new_delivery_point->metro = $delivery_point_data["Metro"];
        $new_delivery_point->address_comment = $delivery_point_data["AddressComment"];
        $new_delivery_point->city_code = $delivery_point_data["CityCode"];
        $new_delivery_point->type = $delivery_point_data["Type"];
        $new_delivery_point->additional_fields = json_encode($delivery_point_data["AdditionalFields"] ?? []);
        $new_delivery_point->is_delete = false;
        $new_delivery_point->hash = $new_delivery_point->getHash();

        return $new_delivery_point;
    }
}
