<?php

namespace Modules\Pvz\Entities;

use App\Components\Dto\BaseDto;

/**
 * Класс-репозиторий для хранения информации о городах, регионах
 *     (технической сопровождающей к списку ПВЗ)
 *
 * @author Oleg Pyatin
 */
class PvzAdditionInfo extends BaseDto
{
    /**
     * Информация о городах
     */
    public $city;
    /**
     * Информация о кодах городов
     */
    public $city_reg;
    /**
     * Информация для карты регионов
     */
    public $regions_map;
    /**
     * Информация полных имен городов
     */
    public $city_full;
    /**
     * Информация о регионах
     */
    public $regions;
    /**
     * Информация о городах (без дубляжа от Пикпоинта, основной код - у СДЕК-а)
     */
    public $city_uniq;
    /**
     * Полная информация о городах (аналогично без дубляжа)
     */
    public $city_full_uniq;
}
