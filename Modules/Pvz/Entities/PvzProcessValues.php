<?php

namespace Modules\Pvz\Entities;

class PvzProcessValues
{
    /**
     * Указание что ПВЗ принадлежат СДЕК-у
     */
    public const CDEK_TYPE_SIGN = 'sdek';
    /**
     * Указание что ПВЗ принадлежат Пикпоинту
     */
    public const PICKPOINT_TYPE_SIGN = 'pickpoint';
    /**
     * Название страны (в новом API не добавляется в отправляемый массив поэтому добавляем вручную)
     */
    public const CDEK_COUNTRY_NAME = 'Россия';

    /**
     * Указание файлового хранилища для сервиса
     */
    public const SAVE_DELIVERY_POINT_FILES_DISK = 'delivery_point';
    /**
     * Название главного файла
     */
    public const MAIN_FILE_NAME = 'delivery_point_full.json';
    /**
     * Название файла c данными о городах
     */
    public const CITY_INFO_FILE_NAME = 'city_info.json';

    /**
     * Массив перечислений кодов основных (выбираемых из меню на сайте) городов в системе СДЕК
     */
    public const MAIN_CITIES_CODES = [
        'Москва'=>'44',
        'Санкт-Петербург'=>'137',
        'Владивосток'=>'288',
        'Волгоград'=>'426',
        'Воронеж'=>'506',
        'Екатеринбург'=>'250',
        'Казань'=>'424',
        'Красноярск'=>'278',
        'Нижний Новгород'=>'414',
        'Новосибирск'=>'270',
        'Омск'=>'268',
        'Пермь'=>'248',
        'Ростов-На-Дону'=>'438',
        'Самара'=>'430',
        'Саратов'=>'428',
        'Уфа'=>'256',
        'Хабаровск'=>'287',
        'Челябинск'=>'259',
    ];

    /**
     * Код города отправки (Москва по-умолчанию, нужен с целью добавления его ПВЗ к любому запросу для корректной
     *     работы виджета)
     */
    public const DEFAULT_CITY_FROM_CODE = '44';

    /**
     * Сообщение об успешном сохранении
     */
    public const SUCCESSFULL_SAVE_MESSAGE = 'Save successfull';

    /**
     * Условное количество ПВЗ для бесконфликтной работы с городом по-умолчанию
     */
    public const DEFAULT_CITY_PVZ_PART_SIZE = 5;
}
