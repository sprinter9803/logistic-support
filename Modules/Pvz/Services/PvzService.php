<?php

namespace Modules\Pvz\Services;

use Modules\Pickpoint\Components\PickpointConnector;
use Modules\Pickpoint\Entities\PickpointProcessValues;
use Modules\Cdek\Components\CdekConnector;
use Modules\Cdek\Entities\CdekProcessValues;
use Modules\Pvz\Repositories\PvzRepository;
use Modules\Pvz\Factories\PvzFactory;
use Modules\Pvz\Entities\PvzProcessValues;
use Modules\Pvz\Entities\PvzAdditionInfo;
use Illuminate\Support\Facades\Storage;
use App\Models\DeliveryPoint;
use Illuminate\Support\Collection;

/**
 * Сервис для работы с объектами ПВЗ
 *
 * @author Oleg Pyatin
 */
class PvzService
{
    protected $pickpoint_connector;

    protected $cdek_connector;

    protected $pvz_repository;

    protected $pvz_factory;

    public function __construct(PickpointConnector $pickpoint_connector, CdekConnector $cdek_connector,
                                PvzRepository $pvz_repository, PvzFactory $pvz_factory)
    {
        $this->pickpoint_connector = $pickpoint_connector;
        $this->cdek_connector = $cdek_connector;

        $this->pvz_repository = $pvz_repository;
        $this->pvz_factory = $pvz_factory;
    }

    public function getAllPvz()
    {
        $json_content = Storage::disk(PvzProcessValues::SAVE_DELIVERY_POINT_FILES_DISK)->get(PvzProcessValues::MAIN_FILE_NAME);
        return response($json_content)->header('Content-Type', 'application/json');
    }


    public function updateCurrentDeliveryPointBase()
    {
        $cdek_delivery_points = $this->cdek_connector->sendSimpleQuery(CdekProcessValues::CDEK_GET_PVZ_LIST_URL);

        $pp_delivery_points = $this->pickpoint_connector->sendSimpleQuery(PickpointProcessValues::GET_POSTAMAT_URL);


        $cdek_points = $this->getCdekDeliveryPoints($cdek_delivery_points);

        $city_info = $this->getCdekCityInfo($cdek_delivery_points);


        $pp_points = $this->getPickpointDeliveryPoints($city_info, $pp_delivery_points);

        $pp_city_info = $this->getPickpointCityInfo($city_info, $pp_delivery_points);

        $external_delivery_points = $cdek_points->merge($pp_points);


        // Получение из репозитория текущей базы
        $current_delivery_points = $this->pvz_repository->getAllDeliveryPointsWithIndexCode();


        // Запуск сохранения для новых
        $new_points = $external_delivery_points->diffKeys($current_delivery_points);   // Посмотреть что external не изменяется

        foreach ($new_points as $new_point_data) {

            if (!$this->pvz_repository->restoreDeliveryPointIfExist($new_point_data["Code"])) {

                $add_point = $this->pvz_factory->createDeliveryPoint($new_point_data);
                $this->pvz_repository->saveNewDeliveryPoint($add_point);
            }
        }

        // Запуск мягкого удаления для старых
        $delete_points = $current_delivery_points->diffKeys($external_delivery_points);

        foreach ($delete_points as $delete_point) {
            $this->pvz_repository->deleteDeliveryPoint($delete_point["id"]);
        }

        // Запуск проверки на изменения (по хэшу) (с перезаписью)
        $coincided_points = $current_delivery_points->intersectByKeys($external_delivery_points);

        foreach ($coincided_points as $point_for_update) {

            $current_code = $point_for_update["code"];

            if ($point_for_update["hash"] !== DeliveryPoint::getPotentialPointHash($external_delivery_points[$current_code])) {

                $this->pvz_repository->updateDeliveryPoint($point_for_update, $external_delivery_points[$current_code]);
            }
        }

        return $pp_city_info;
    }


    public function getCdekDeliveryPoints(array $cdek_delivery_points)
    {
        $prepared_delivery_points_array = [];

        foreach ($cdek_delivery_points as $delivery_point) {

            $prepared_point = [
                'Code'=>(string)$delivery_point["code"],
                'Name'=>(string)($delivery_point["name"] ?? ''),
                'WorkTime'=>(string)($delivery_point["work_time"] ?? ''),
                'Address'=>(string)$delivery_point["location"]["address"],
                'Phone'=>(string)($delivery_point["phones"][0]["number"] ?? ''),
                'Note'=>(string)($delivery_point["note"] ?? ''),
                'cX'=>(string)$delivery_point["location"]["longitude"],
                'cY'=>(string)$delivery_point["location"]["latitude"],
                'Dressing'=>(($delivery_point["is_dressing_room"] ?? false)=='true'),
                'Cash'=>(($delivery_point["have_cashless"] ?? false)=='true'),
                'Station'=>(string)($delivery_point["nearest_station"] ?? ''),
                'Site'=>(string)($delivery_point["site"] ?? ''),
                'Metro'=>(string)($delivery_point["nearest_metro_station"] ?? ''),
                'AddressComment'=>(string)($delivery_point["address_comment"] ?? ''),
                'CityCode'=>(string)$delivery_point["location"]["city_code"],
                'Type'=>PvzProcessValues::CDEK_TYPE_SIGN, 
                'AdditionalFields'=>[]
            ];

            if (isset($delivery_point["weight_min"]) && isset($delivery_point["weight_max"])) {
                $prepared_point['AdditionalFields'] += [
                    'WeightLim'=>[
                        'MIN'=>(float)($delivery_point["weight_min"]),
                        'MAX'=>(float)($delivery_point["weight_max"]),
                    ]
                ];
            }

            if (isset($delivery_point["office_image_list"])) {

                $image_set = [];

                foreach ($delivery_point["office_image_list"] as $image) {
                    // ! Временно отключаем - в тестовом варианте выдается неправильный формат
                    //if (strstr($_tmpUrl = (string)$image['url'], 'http') === false) { continue; }
                    $image_set[] = (string)$image['url'];
                }

                if (count($image_set = array_filter($image_set))) {
                    $prepared_point['AdditionalFields'] += [
                        'Picture' => $image_set
                    ];
                }
            }

            $prepared_delivery_points_array[] = $prepared_point;
        }

        $prepared_delivery_points = collect($prepared_delivery_points_array);

        return $prepared_delivery_points->keyBy('Code');
    }


    public function getCdekCityInfo(array $cdek_delivery_points)
    {
        $city_set = [];
        $city_reg_set = [];
        $regionsmap_set = [];
        $cityfull_set = [];
        $regions_set = [];

        foreach ($cdek_delivery_points as $delivery_point) {

            $city_code = (string)$delivery_point["location"]["city_code"];
            $region_code = (int)$delivery_point["location"]["region_code"];

            $city = (string)$delivery_point["location"]["city"];

            if (strpos($city, '(') !== false) {
                $city = trim(substr($city, 0, strpos($city, '(')));
            }
            if (strpos($city, ',') !== false) {
                $city = trim(substr($city, 0, strpos($city, ',')));
            }

            if (!array_key_exists($city_code, $city_set)) {
                $city_set[$city_code] = $city;
                $city_reg_set[$city_code] = $region_code;
                $regionsmap_set[$region_code][] = (int)$city_code;
                $cityfull_set[$city_code] = PvzProcessValues::CDEK_COUNTRY_NAME . ' ' . (string)$delivery_point["location"]["region"] . ' ' . $city;
                $regions_set[$city_code] = implode(', ', array_filter([(string)$delivery_point["location"]["region"], PvzProcessValues::CDEK_COUNTRY_NAME]));
            }
        }

        return PvzAdditionInfo::loadFromArray([
            'city'=>$city_set,
            'city_reg'=>$city_reg_set,
            'regions_map'=>$regionsmap_set,
            'city_full'=>$cityfull_set,
            'regions'=>$regions_set,
            'city_uniq'=>$city_set,
            'city_full_uniq'=>$cityfull_set,
        ]);
    }


    public function getPickpointCityInfo(PvzAdditionInfo $cdek_regions_info, array $pickpoint_delivery_points)
    {
        $city_set = $cdek_regions_info->city;
        $city_reg_set = $cdek_regions_info->city_reg;
        $regionsmap_set = $cdek_regions_info->regions_map;
        $cityfull_set = $cdek_regions_info->city_full;
        $regions_set = $cdek_regions_info->regions;

        $city_uniq_set = $cdek_regions_info->city;
        $city_full_uniq_set = $cdek_regions_info->city_full;

        $sdek_city_regions = array_flip($cdek_regions_info->city);

        foreach ($pickpoint_delivery_points as $delivery_point) {

            $city_code = '';

            $city = (string)$delivery_point['CitiName'];

            if (strpos($city, '(') !== false) {
                $city = trim(substr($city, 0, strpos($city, '(')));
            }
            if (strpos($city, ',') !== false) {
                $city = trim(substr($city, 0, strpos($city, ',')));
            }

            if (!isset($cdek_regions_info->city[$city]) || empty($cdek_regions_info->city)) {
                $city_code = (string)$delivery_point['CitiId'];
                if (isset($cdek_regions_info->city[(int)$delivery_point['CitiId']])) {
                    $city_code .= '0000';
                }
            } else {
                $city_code = $cdek_regions_info->city[$city];
                $city = $cdek_regions_info->city[$city_code];
            }

            if (!array_key_exists($city_code, $city_set)) {
                $city_set[$city_code] = $city;
                $city_reg_set[(int)$delivery_point['CitiOwnerId']] = $city_code;
                $regionsmap_set[(int)$delivery_point['CitiOwnerId']][] = (int)$city_code;
                $cityfull_set[$city_code] = (string)$delivery_point['CountryName'] . ' ' . (string)$delivery_point['Region'] . ' ' . $city;
                $regions_set[$city_code] = implode(', ', array_filter(array((string)$delivery_point['Region'], (string)$delivery_point['CountryName'])));

                if (empty($sdek_city_regions[$city])) {
                    $city_uniq_set[$city_code] = $city;
                    $city_full_uniq_set[$city_code] = (string)$delivery_point['CountryName'] . ' ' . (string)$delivery_point['Region'] . ' ' . $city;
                }
            }
        }

        return PvzAdditionInfo::loadFromArray([
            'city'=>$city_set,
            'city_reg'=>$city_reg_set,
            'regions_map'=>$regionsmap_set,
            'city_full'=>$cityfull_set,
            'regions'=>$regions_set,
            'city_uniq'=>$city_uniq_set,
            'city_full_uniq'=>$city_full_uniq_set,
        ]);
    }




    public function getPickpointDeliveryPoints(PvzAdditionInfo $city_info, array $pp_delivery_points)
    {
        $prepared_pp_points_array = [];

        $pp_point = $pp_delivery_points[0];

        foreach ($pp_delivery_points as $delivery_point) {

            $city_code = '';

            $city = (string)$delivery_point['CitiName'];

            if (strpos($city, '(') !== false) {
                $city = trim(substr($city, 0, strpos($city, '(')));
            }
            if (strpos($city, ',') !== false) {
                $city = trim(substr($city, 0, strpos($city, ',')));
            }

            if (!isset($city_info->city[$city]) || empty($city_info->city)) {
                $city_code = (string)$delivery_point['CitiId'];
                if (isset($city_info->city[(int)$delivery_point['CitiId']])) {
                    $city_code .= '0000';
                }
            } else {
                $city_code = $city_info->city[$city];
                $city = $city_info->city[$city_code];
            }

            $code = (string)$delivery_point['Number'];

            $prepared_point = [
                'Code'=>$code,
                'Name'=>(string)($delivery_point["Name"] ?? ''),
                'WorkTime'=>(string)($delivery_point["WorkTimeSms"] ?? ''),
                'Address'=>(string)$delivery_point["Address"],
                'Phone'=>'',    // Проставляется пустым как и в исходном скрипте
                'Note'=>(string)($delivery_point["InDescription"] ?? ''),
                'cX'=>(string)$delivery_point["Longitude"],
                'cY'=>(string)$delivery_point["Latitude"],
                'Dressing'=>(($delivery_point["Fitting"] ?? false)=='true'),
                'Cash'=>(($delivery_point["Cash"] ?? false)=='true'),
                'Station'=>(string)($delivery_point["Metro"] ?? ''),
                'Site'=>null,   // Для Пикпоинта в скриптах указывается false, здесь ставим null
                'Metro'=>(string)implode(', ',  ($delivery_point["MetroArray"] ?? [])),
                'AddressComment'=>(string)($delivery_point["OutDescription"] ?? ''),
                'CityCode'=>$city_code,
                'Type'=>PvzProcessValues::PICKPOINT_TYPE_SIGN,
            ];

            $prepared_point += [
                'AdditionalFields'=>[
                    'TypePP' => (($delivery_point['TypeTitle'] ?? '') === 'ПВЗ') ? 'PVZ' : 'APT',
                    'Status' => (string)($delivery_point["Status"] ?? '')
                ]
            ];

            if (isset($delivery_point["MaxWeight"])) {
                $prepared_point['AdditionalFields'] += [
                    'WeightLim'=>[
                        'MAX'=>(float)$delivery_point['MaxWeight']
                    ]
                ];
            }

            if (isset($delivery_point['OutDescription'])) {
                $prepared_point['AdditionalFields'] += [
                    'Path'=>(string)$delivery_point['OutDescription']
                ];
            }

            $pp_imgs_array = [];

            if (isset($delivery_point['FileI0'])) {
                $pp_imgs_array[] = '//e-solution.pickpoint.ru/api/' . $delivery_point['FileI0'];
            }
            if (isset($delivery_point['FileI1'])) {
                $pp_imgs_array[] = '//e-solution.pickpoint.ru/api/' . $delivery_point['FileI1'];
            }
            if (isset($delivery_point['FileI2'])) {
                $pp_imgs_array[] = '//e-solution.pickpoint.ru/api/' . $delivery_point['FileI2'];
            }

            if (count($pp_imgs_array = array_filter($pp_imgs_array))) {
                $prepared_point['AdditionalFields'] += [
                    'Picture'=>$pp_imgs_array
                ];
            }

            $prepared_delivery_points_array[] = $prepared_point;
        }

        $prepared_delivery_points = (collect($prepared_delivery_points_array))->keyBy('Code');

        return $prepared_delivery_points;
    }

    public function getAllDeliveryPoints()
    {
        $current_delivery_points = $this->pvz_repository->getAllDeliveryPointsByRegions();

        $prepared_delivery_points = $current_delivery_points->map(function ($item, $key) {

            $item = $item->map(function ($delivery_point, $key) {

                $additional_fields = json_decode($delivery_point["additional_fields"], true);

                $delivery_point = [
                    'Code'=>$delivery_point["code"],
                    'Name'=>$delivery_point["name"],
                    'WorkTime'=>$delivery_point["work_time"],
                    'Address'=>$delivery_point["address"],
                    'Phone'=>$delivery_point["phone"],
                    'Note'=>$delivery_point["note"],
                    'cX'=>$delivery_point["cx"],
                    'cY'=>$delivery_point["cy"],
                    'Dressing'=>($delivery_point["dressing"]) ? true : false,
                    'Cash'=>($delivery_point["cash"]) ? true : false,
                    'Station'=>$delivery_point["station"],
                    'Site'=>$delivery_point["site"],
                    'Metro'=>$delivery_point["metro"],
                    'AddressComment'=>$delivery_point["address_comment"],
                    'CityCode'=>$delivery_point["city_code"],
                    'Type'=>$delivery_point["type"]
                ];

                $delivery_point += $additional_fields;

                return $delivery_point;
            });

            return $item->keyBy('Code');
        });

        return $prepared_delivery_points->toArray();
    }

    public function getDeliveryPointsForCityByCode(string $city_code)
    {

        $city_delivery_points = $this->pvz_repository->getDeliveryPointsByCityCode($city_code);

        $prepared_delivery_points = $city_delivery_points->map(function ($delivery_point, $key) {

            $additional_fields = json_decode($delivery_point["additional_fields"], true);

            $delivery_point = [
                'Code'=>$delivery_point["code"],
                'Name'=>$delivery_point["name"],
                'WorkTime'=>$delivery_point["work_time"],
                'Address'=>$delivery_point["address"],
                'Phone'=>$delivery_point["phone"],
                'Note'=>$delivery_point["note"],
                'cX'=>$delivery_point["cx"],
                'cY'=>$delivery_point["cy"],
                'Dressing'=>($delivery_point["dressing"]) ? true : false,
                'Cash'=>($delivery_point["cash"]) ? true : false,
                'Station'=>$delivery_point["station"],
                'Site'=>$delivery_point["site"],
                'Metro'=>$delivery_point["metro"],
                'AddressComment'=>$delivery_point["address_comment"],
                'CityCode'=>$delivery_point["city_code"],
                'Type'=>$delivery_point["type"]
            ];

            $delivery_point += $additional_fields;

            return $delivery_point;
        });

        return ($prepared_delivery_points->keyBy('Code'))->toArray();
    }

    public function getDeliveryPointsForCity(string $city_name)
    {
        // Шаг 1 - Пробуем выдать кеш если попался один из основных городов
        if (isset(PvzProcessValues::MAIN_CITIES_CODES[$city_name])) {
            $json_content = Storage::disk(PvzProcessValues::SAVE_DELIVERY_POINT_FILES_DISK)->get(PvzProcessValues::MAIN_CITIES_CODES[$city_name].'.json');
            return response($json_content)->header('Content-Type', 'application/json');
        }

        // Шаг 2 - Если запрашиваемый город не среди основных - делаем обращение к базе
        $city_info_json = Storage::disk(PvzProcessValues::SAVE_DELIVERY_POINT_FILES_DISK)->get(PvzProcessValues::CITY_INFO_FILE_NAME);

        $city_info_data = json_decode($city_info_json, true);

        $city_similar_name_set = [];

        foreach ($city_info_data["CITY_DUB"] as $code=>$name) {
            if ($name === $city_name) {
                $city_similar_name_set[] = $code;
            }
        }

        // Код который закреплен за городом в уникальном списке
        $main_city_code = array_search($city_name, $city_info_data["CITY"]);

        if (!empty($city_similar_name_set)) {

            $default_city_json = Storage::disk(PvzProcessValues::SAVE_DELIVERY_POINT_FILES_DISK)->get(PvzProcessValues::DEFAULT_CITY_FROM_CODE.'.json');
            $default_city_data = json_decode($default_city_json, true);
            $default_city_points = array_slice($default_city_data["pickpoint"]["PVZ"][PvzProcessValues::DEFAULT_CITY_FROM_CODE], 0, PvzProcessValues::DEFAULT_CITY_PVZ_PART_SIZE);

            $answer_data = [
                'pickpoint'=>[
                    'PVZ'=>[
                        $main_city_code=>[]
                    ],
                    'CITY'=>$city_info_data["CITY"],
                    'REGIONS'=>$city_info_data["REGIONS"],
                    'CITYFULL'=>$city_info_data["CITYFULL"],
                    'COUNTRIES'=>[],
                    'CITYREG'=>$city_info_data["CITYREG"],
                    'REGIONSMAP'=>$city_info_data["REGIONSMAP"]
                ]
            ];

            foreach ($city_similar_name_set as $city_code) {
                $answer_data["pickpoint"]['PVZ'][$main_city_code] += $this->getDeliveryPointsForCityByCode($city_code);
            }

            $answer_data["pickpoint"]['PVZ'] += [
                PvzProcessValues::DEFAULT_CITY_FROM_CODE=>$default_city_points
            ];

            return response()->json($answer_data);

        } else {

            // Шаг 3 - Если такого города не нашлось в базе - отправляем полный файл
            $json_content = Storage::disk(PvzProcessValues::SAVE_DELIVERY_POINT_FILES_DISK)->get(PvzProcessValues::MAIN_FILE_NAME);
            return response($json_content)->header('Content-Type', 'application/json');
        }
    }
}
