<?php

namespace Modules\Pvz\Services;

use Illuminate\Support\Facades\Storage;
use Modules\Pvz\Entities\PvzProcessValues;
use Modules\Pvz\Services\PvzService;

/**
 * Сервис для выполнения вспомогательных действий с ПВЗ
 *
 * @author Oleg Pyatin
 */
class PvzManageService
{
    protected $pvz_service;

    public function __construct(PvzService $pvz_service)
    {
        $this->pvz_service = $pvz_service;
    }

    public function dataManageQuery()
    {
        $pp_city_info = $this->pvz_service->updateCurrentDeliveryPointBase();

        $full_file = json_encode([
            'pickpoint'=>[
                'PVZ'=>$this->pvz_service->getAllDeliveryPoints(),
                'CITY'=>$pp_city_info->city_uniq,
                'REGIONS'=>$pp_city_info->regions,
                'CITYFULL'=>$pp_city_info->city_full_uniq,
                'COUNTRIES'=>[],
                'CITYREG'=>$pp_city_info->city_reg,
                'REGIONSMAP'=>$pp_city_info->regions_map
            ]
        ]);

        $city_info_file = json_encode([
            'CITY'=>$pp_city_info->city_uniq,
            'REGIONS'=>$pp_city_info->regions,
            'CITYFULL'=>$pp_city_info->city_full_uniq,
            'COUNTRIES'=>[],
            'CITYREG'=>$pp_city_info->city_reg,
            'REGIONSMAP'=>$pp_city_info->regions_map,
            'CITY_DUB'=>$pp_city_info->city,
            'CITY_FULL_DUB'=>$pp_city_info->city_full
        ]);

        Storage::disk(PvzProcessValues::SAVE_DELIVERY_POINT_FILES_DISK)->put(PvzProcessValues::MAIN_FILE_NAME, $full_file);

        Storage::disk(PvzProcessValues::SAVE_DELIVERY_POINT_FILES_DISK)->put(PvzProcessValues::CITY_INFO_FILE_NAME, $city_info_file);

        // Сохранение кешей
        foreach (PvzProcessValues::MAIN_CITIES_CODES as $city_name=>$city_code) {

            $main_city_data = [
                'pickpoint'=>[
                    'PVZ'=>[
                        $city_code=>[]//$this->pvz_service->getDeliveryPointsForCityByCode($city_code),
                    ],
                    'CITY'=>$pp_city_info->city_uniq,
                    'REGIONS'=>$pp_city_info->regions,
                    'CITYFULL'=>$pp_city_info->city_full_uniq,
                    'COUNTRIES'=>[],
                    'CITYREG'=>$pp_city_info->city_reg,
                    'REGIONSMAP'=>$pp_city_info->regions_map
                ]
            ];

            $city_similar_name_set = [];
            foreach ($pp_city_info->city as $code=>$name) {
                if ($name === $city_name) {
                    $city_similar_name_set[] = $code;
                }
            }

            foreach ($city_similar_name_set as $code) {
                $main_city_data["pickpoint"]['PVZ'][$city_code] += $this->pvz_service->getDeliveryPointsForCityByCode($code);
            }

            if ($city_code !== PvzProcessValues::DEFAULT_CITY_FROM_CODE) {
                $main_city_data["pickpoint"]['PVZ'] += [
                    PvzProcessValues::DEFAULT_CITY_FROM_CODE => $this->pvz_service->getDeliveryPointsForCityByCode(PvzProcessValues::DEFAULT_CITY_FROM_CODE)
                ];
            }

            Storage::disk(PvzProcessValues::SAVE_DELIVERY_POINT_FILES_DISK)->put($city_code.'.json', json_encode($main_city_data));
        }

        echo PvzProcessValues::SUCCESSFULL_SAVE_MESSAGE;
    }
}
