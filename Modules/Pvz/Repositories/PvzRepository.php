<?php

namespace Modules\Pvz\Repositories;

use App\Models\DeliveryPoint;

/**
 * Класс-репозиторий для работы с БД в аспекте заказов
 *
 * @author Oleg Pyatin
 */
class PvzRepository
{
    /**
     * Функция сохранения новой записи о ПВЗ
     *
     * @param DeliveryPoint $new_delivery_point  Заполненная запись о создания нового ПВЗ в системе
     * @return bool  Сохранилось или нет
     */
    public function saveNewDeliveryPoint(DeliveryPoint $new_delivery_point)
    {
        return $new_delivery_point->save();
    }

    /**
     * Функция получения списка ПВЗ индексированных их кодом
     *
     * @return Collection  Список ПВЗ
     */
    public function getAllDeliveryPointsWithIndexCode()
    {
        $delivery_points = DeliveryPoint::where('is_delete', false)->get();

        return (collect($delivery_points->toArray()))->keyBy('code');
    }

    /**
     * Функция восстановления ПВЗ если он был получен в новом блоке данных
     *
     * @return Collection  Список ПВЗ
     */
    public function restoreDeliveryPointIfExist(string $restore_point_code)
    {
        $restore_delivery_point = DeliveryPoint::where('is_delete', true)
                                        ->where('code', $restore_point_code)
                                        ->first();

        if (!empty($restore_delivery_point)) {
            $restore_delivery_point->is_delete=false;
            $restore_delivery_point->save();
            return true;
        } else {
            return false;
        }
    }

    /**
     * Soft-delete для одного из пунктов доставки
     *
     * @return void  Просто удаляем, ничего не возвращаем
     */
    public function deleteDeliveryPoint(string $delete_delivery_point_id)
    {
        $delete_delivery_point = DeliveryPoint::find($delete_delivery_point_id);
        $delete_delivery_point->is_delete = true;
        $delete_delivery_point->save();
    }

    /**
     * Перезапись полей для пункта доставки в случае его обновления на внешнем ресурск
     *
     * @return void  Просто сохраняем ничего не возвращаем
     */
    public function updateDeliveryPoint(array $point_for_update, array $new_delivery_point)
    {
        $point_for_update = DeliveryPoint::find($point_for_update["id"]);

        $point_for_update->name = $new_delivery_point["Name"];
        $point_for_update->work_time = $new_delivery_point["WorkTime"];
        $point_for_update->address = $new_delivery_point["Address"];
        $point_for_update->phone = $new_delivery_point["Phone"];
        $point_for_update->note = $new_delivery_point["Note"];
        $point_for_update->cx = $new_delivery_point["cX"];
        $point_for_update->cy = $new_delivery_point["cY"];
        $point_for_update->dressing = $new_delivery_point["Dressing"];
        $point_for_update->cash = $new_delivery_point["Cash"];
        $point_for_update->station = $new_delivery_point["Station"];
        $point_for_update->site = $new_delivery_point["Site"];
        $point_for_update->metro = $new_delivery_point["Metro"];
        $point_for_update->address_comment = $new_delivery_point["AddressComment"];
        $point_for_update->city_code = $new_delivery_point["CityCode"];
        $point_for_update->type = $new_delivery_point["Type"];
        $point_for_update->additional_fields = json_encode($new_delivery_point["AdditionalFields"] ?? []);
        $point_for_update->is_delete = false;
        $point_for_update->hash = $point_for_update->getHash();

        $point_for_update->save();
    }

    public function getAllDeliveryPointsByRegions()
    {
        $delivery_points = DeliveryPoint::where('is_delete', false)->get();
        return (collect($delivery_points->toArray()))->groupBy('city_code');
    }

    public function getDeliveryPointsByCityCode(string $city_code)
    {
        $delivery_points = DeliveryPoint::where('is_delete', false)
                                            ->where('city_code', $city_code)
                                            ->get();

        return (collect($delivery_points->toArray()));
    }
}
