<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'pvz'], function () {
    Route::get('all', 'PvzController@getAllPvz')->name('pvz.all');
    Route::get('city', 'PvzController@getCityPvz')->name('pvz.city');
    Route::get('update', 'PvzController@updatePvz')->name('pvz.update');
});

