<?php

namespace Modules\Pickpoint\Entities;

class PickpointProcessValues
{
    /**
     * URL для получения списка ПВЗ
     */
    public const GET_POSTAMAT_URL = 'postamatlist';
}
