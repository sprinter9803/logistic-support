<?php

namespace Modules\Pickpoint\Components;

use Illuminate\Support\Facades\Http;

class PickpointConnector
{
    public function sendSimpleQuery(string $uri)
    {
        $response = Http::withOptions([
            'verify'=>false
        ])->get(getenv("PICKPONT_BASE_URL").$uri);

        return $response->json();
    }
}
