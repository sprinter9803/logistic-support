<?php

namespace Modules\Cdek\Entities;

class CdekProcessValues
{
    /**
     * Время жизни токена доступа в системме CDEK
     */
    public const CDEK_ACCESS_TOKEN_LIFETIME = 3600;
    /**
     * URL для получения списка ПВЗ
     */
    public const CDEK_GET_PVZ_LIST_URL = 'deliverypoints?type=ALL&lang=rus';
    /**
     * URL-адрес получения тестовых авторизационных данных для CDEK
     */
    public const CDEK_EDU_AUTH_URI = 'oauth/token?parameters';
}
