<?php

namespace Modules\Cdek\Components;

use Illuminate\Support\Facades\Http;
use App\Models\CdekAccessToken;
use Modules\Cdek\Entities\CdekProcessValues;
use Carbon\Carbon;

/**
 * Компонент для выполнения запросов к сервису Cdek
 *
 * @author Oleg Pyatin
 */
class CdekConnector
{
    public function sendDataQuery(string $uri, array $data)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. $this->getAuthToken()
        ])->withOptions([
            'verify'=>false
        ])->post(getenv("CDEK_BASE_URL").$uri, $data)->json();

        return $response;
    }

    public function sendSimpleQuery(string $uri)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. $this->getAuthToken()
        ])->withOptions([
            'verify'=>false
        ])->get(getenv("CDEK_BASE_URL").$uri)->json();

        return $response;
    }

    public function processDataQuery(string $uri)
    {
        $response = Http::withHeaders([
            'Authorization'=>' Bearer '. $this->getAuthToken()
        ])->withOptions([
            'verify'=>false
        ])->get(getenv("CDEK_BASE_URL").$uri);

        return $response->getBody()->getContents();
    }

    public function getAuthToken()
    {
        if ($accessToken = $this->tryGetTokenFromDatabase()) {
            return $accessToken;
        } else {
            $accessToken = $this->tryGetTokenFromService();
            // Здесь еще сохраняем токен в базу (получив его от сервиса)
            $this->saveNewTokenIntoDB($accessToken);
            return $accessToken;
        }
    }

    private function tryGetTokenFromDatabase()
    {
        $lastToken = CdekAccessToken::orderByDesc('created_at')->first();

        if (empty($lastToken)) {
            return false;
        }

        $timeLastToken = Carbon::parse($lastToken->created_at)->timestamp;
        $currentTime = time();

        if ($currentTime - $timeLastToken > CdekProcessValues::CDEK_ACCESS_TOKEN_LIFETIME) {
            // Возвращаем false - это говорит о том, что нужно брать токен уже из сервиса
            return false;
        } else {
            return $lastToken->token;
        }
    }

    private function tryGetTokenFromService(): string
    {
        $response = Http::asForm()->withOptions([
            'verify'=>false
        ])->post(getenv("CDEK_BASE_URL").CdekProcessValues::CDEK_EDU_AUTH_URI, [
            'grant_type'=>'client_credentials',
            'client_id'=>getenv("CDEK_CLIENT_ID"),
            'client_secret'=>getenv("CDEK_CLIENT_SECRET")
        ]);

//        print_r($response);
//        exit();

        return $response["access_token"];
    }

    private function saveNewTokenIntoDB(string $accessToken): void
    {
        $newDownloadedToken = new CdekAccessToken();
        $newDownloadedToken->token=$accessToken;
        $newDownloadedToken->created_at=(string)time();
        $newDownloadedToken->save();
    }
}
