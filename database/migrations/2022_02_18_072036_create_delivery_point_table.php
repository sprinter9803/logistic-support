<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDeliveryPointTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('delivery_point', function (Blueprint $table) {
            $table->id();
            $table->string('code');
            $table->string('name')->nullable();
            $table->string('city_code')->nullable();
            $table->string('work_time')->nullable();
            $table->string('address')->nullable();
            $table->string('phone')->nullable();
            $table->text('note')->nullable();
            $table->string('cx')->nullable();
            $table->string('cy')->nullable();
            $table->boolean('dressing')->nullable();
            $table->boolean('cash')->nullable();
            $table->text('station')->nullable();
            $table->string('site')->nullable();
            $table->text('metro')->nullable();
            $table->text('address_comment')->nullable();
            $table->string('type')->nullable();
            $table->text('additional_fields')->nullable();
            $table->boolean('is_delete');
            $table->string('hash')->nullable();
            $table->timestamps();
        });

        Schema::table('delivery_point', function (Blueprint $table) {
            $table->index('city_code');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('delivery_point', function (Blueprint $table) {
            $table->dropIndex('delivery_point_city_code_index');
        });


        Schema::dropIfExists('delivery_point');
    }
}
